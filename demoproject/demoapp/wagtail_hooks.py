from wagtail.contrib.modeladmin.options import ModelAdmin, modeladmin_register
from .models import SampleModel


class SampleModelAdmin(ModelAdmin):
    model = SampleModel
    menu_label = "Sample Model"
    menu_icon = "fa-cogs"
    # menu_order = 200
    add_to_settings_menu = False
    exclude_from_explorer = False
    list_display = ("char_field", "integer_field", "text_field")
    search_fields = ("char_field", "integer_field", "text_field")


modeladmin_register(SampleModelAdmin)
