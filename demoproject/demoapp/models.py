from django.db import models
from wagtail.admin.panels import FieldPanel


# Create your models here.
class SampleModel(models.Model):
    char_field = models.CharField(max_length=128)
    integer_field = models.IntegerField()
    text_field = models.TextField()

    panels = [
        FieldPanel("title"),
        FieldPanel("author"),
        FieldPanel("cover_photo"),
    ]

    def __str__(self):
        return str(self.char_field)

    class Meta:
        verbose_name = "Sample model"
        verbose_name_plural = "Sample models"
