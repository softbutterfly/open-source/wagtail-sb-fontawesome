# Generated by Django 4.2.1 on 2023-05-10 18:55

from django.db import migrations, models


class Migration(migrations.Migration):
    initial = True

    dependencies = []

    operations = [
        migrations.CreateModel(
            name="SampleModel",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("char_field", models.CharField(max_length=128)),
                ("integer_field", models.IntegerField()),
                ("text_field", models.TextField()),
            ],
            options={
                "verbose_name": "Sample model",
                "verbose_name_plural": "Sample models",
            },
        ),
    ]
