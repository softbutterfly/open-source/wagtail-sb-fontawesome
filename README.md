![Community-Project](https://gitlab.com/softbutterfly/open-source/open-source-office/-/raw/master/banners/softbutterfly-open-source--banner--community-project.png)

![PyPI - Supported versions](https://img.shields.io/pypi/pyversions/wagtail-sb-fontawesome)
![PyPI - Package version](https://img.shields.io/pypi/v/wagtail-sb-fontawesome)
![PyPI - Downloads](https://img.shields.io/pypi/dm/wagtail-sb-fontawesome)
![PyPI - MIT License](https://img.shields.io/pypi/l/wagtail-sb-fontawesome)

[![Codacy Badge](https://app.codacy.com/project/badge/Grade/2f6661a360234960b1800c0bdac37d3c)](https://app.codacy.com/gl/softbutterfly/wagtail-sb-fontawesome/dashboard?utm_source=gl&utm_medium=referral&utm_content=&utm_campaign=Badge_grade)

# Wagtail FontAwesome

Use FontAwesome icons in Wagtail admin.

## Requirements

- Python 3.8.1 or higher
- Django 4.0.0 or higher
- Wagtail 4.0.0 or higher

## Install

```bash
pip install wagtail-sb-fontawesome
```

## Usage

Add `wagtail_sb_fontawesome` to your `INSTALLED_APPS` settings

```
INSTALLED_APPS = [
  # ...
  "wagtail_sb_fontawesome",
]
```

## Docs

- [Ejemplos](https://gitlab.com/softbutterfly/open-source/wagtail-sb-fontawesome/-/wikis)
- [Wiki](https://gitlab.com/softbutterfly/open-source/wagtail-sb-fontawesome/-/wikis)

## Changelog

All changes to versions of this library are listed in the [change history](CHANGELOG.md).

## Development

Check out our [contribution guide](CONTRIBUTING.md).

## Contributors

See the list of contributors [here](https://gitlab.com/softbutterfly/open-source/wagtail-sb-fontawesome/-/graphs/develop).
