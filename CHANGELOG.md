# Changelog

## [Unreleased]

## [0.3.0] - 2023-05-10

* Add support for wagtail 5.x

## [0.2.0] - 2023-04-29

* Add compat module to fix import warning

## [0.1.0] - 2023-04-15

* Use of FontAwesome 4.7.0 in the wagtail admin
